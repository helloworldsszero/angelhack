const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const compress = require('compression')
const methodOverride = require('method-override')
const cors = require('cors')
const httpStatus = require('http-status')
const passport = require('./passport')
const routes = require('../server/routes/index.route')
const config = require('./config')
const APIError = require('../server/helpers/APIError')
const Path = require('path')
const fs = require('fs')

const User = require('../server/models/user.model')

// require passport

const app = express()

// parse body params and attache them to req.body
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
require('./passport')

// enable CORS - Cross Origin Resource Sharing
app.use(cors())
app.use(passport.initialize())

app.set('view options', {layout: false})
const p = __dirname.replace('config', '')
app.use(express.static(p + '/smart_contract'))

app.use('/login', (req, res) => {
  fs.readFile(p + '/smart_contract/html/login.html', 'utf8', (err, txt) => {
    res.send(txt)
  })
})

app.use('/register', (req, res) => {
  fs.readFile(p + '/smart_contract/html/register.html', 'utf8', (err, txt) => {
    res.send(txt)
  })
})
// mount all routes on /api path
app.use('/api', routes)

// if error is not an instanceOf APIError, convert it.
app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ')
    const error = new APIError(unifiedErrorMessage, err.status, true)
    return next(error)
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic)
    return next(apiError)
  }
  return next(err)
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND)
  return next(err)
})

// log error in winston transports except when executing test suite

// error handler, send stacktrace only during development
app.use((err, req, res, next) => // eslint-disable-line no-unused-vars
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
    stack: ['development', 'test'].includes(config.env) ? err.stack : {}
  }))

module.exports = app
