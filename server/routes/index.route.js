const express = require('express')

const router = express.Router()
const loginRoute = require('./login.route')

/** GET /health-check - Check service health */
router.get('/ping', (req, res) => {
  res.send('pong')
})

// mount user routes at /users
router.use('/login', loginRoute)

module.exports = router
