const express = require('express');
const app = express();
const fs = require('fs');
const mongoose = require("mongoose");
var bodyParser = require('body-parser')
mongoose.connect('mongodb://localhost/vpdata');
var hash = require('js-sha3');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("DB connected");
});

var cookieParser = require('cookie-parser');
app.use(cookieParser())

var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.get('/login', (req, res) => {
    res.send(fs.readFileSync("./html/login.html", "utf8"));
});

app.get('/register', (req, res) => {
    res.send(fs.readFileSync("./html/register.html", "utf8"));
});

//Performs token check and returns user account information
async function performTokenCheckAndLoadPage(req, res, htmlDoc)
{
    if(req.cookies["usertoken"] == null)
    {
        res.redirect("/login");
    }
    else
    {
        //check if token is in db
        var result = db.collection("tokens").findOne({token: req.cookies["usertoken"]}, (error, val)=>{ 
            if(!val)
            {
                res.clearCookie("usertoken");
                res.redirect("/login");
            }
            else
            {
                //Find associated account
                db.collection("accounts").findOne({email: val.email}, (error, value)=>{ 
                    var html = fs.readFileSync(htmlDoc, "utf8");
                    for(x in value)
                    {
                        html = html.replace("{" + x + "}", value[x]);
                    }
                    res.send(html);
                });
            }
        });
    }
}

app.get("/dashboard", (req, res)=>{
    //check if token is in a cookie
    performTokenCheckAndLoadPage(req, res, "./html/dashboard.html");

}); 

app.post('/auth', urlencodedParser, (req, res)=>{
    if (!req.body) return res.sendStatus(400);
    if(!req.body.email.includes("@")) return res.sendStatus(400);
    var email = req.body.email;
    var result = db.collection("accounts").findOne({email: req.body.email}, (error, val)=>{ 
        if(val == undefined)
        {
            var r = fs.readFileSync("./html/regstat.html", "utf8").toString();
            r = r.replace("{messageTitle}", "Log in failed");
            r = r.replace("{messageContents}", "Your account was not found.<br><br> <a href='/login'>Go back<a>");
            res.send(r);
            return;
        }
        else
        {
            //check if password is correct
            if(hash.keccak256(req.body.password) != val.passwordHash)
            {
                var r = fs.readFileSync("./html/regstat.html", "utf8").toString();
                r = r.replace("{messageTitle}", "Log in failed");
                r = r.replace("{messageContents}", "Your entered an invalid password.<br><br> <a href='/login'>Go back<a>");
                res.send(r);
                return;
            }
            require('crypto').randomBytes(128, function(err, buffer) {
                var token = buffer.toString('hex');
                var tok = {
                    token: token,
                    email: email
                };
                db.collection("tokens").insert(tok);
                res.cookie("usertoken", token, {expires: new Date(Date.now() + (60 * 60 * 24))});
                res.redirect("/dashboard");
            });
        }
    });
});

app.get("/defaultavatar", (req, res)=>{
    res.sendFile(__dirname + "/default.svg");
});

app.post('/createAccount', urlencodedParser, (req, res)=>{
    if (!req.body) return res.sendStatus(400);
    var email = req.body.email;
    if(req.body.password.length < 10)
    {
        var r = fs.readFileSync("./html/regstat.html", "utf8").toString();
        r = r.replace("{messageTitle}", "Account Creation Failed");
        r = r.replace("{messageContents}", "Password does not meet minimum length.");
        res.send(r);
        return;
    }
    var result = db.collection("accounts").findOne({email: email}, (error, val)=>{ 
        if(val != undefined)
        {
            var r = fs.readFileSync("./html/regstat.html", "utf8").toString();
            r = r.replace("{messageTitle}", "Account Creation Failed");
            r = r.replace("{messageContents}", "An account already exists with that email.<br>You can reset your password <a href=''>here<a>");
            res.send(r);
            return;
        }
        else
        {
            var uinfo = {
                email: req.body.email,
                fullName: req.body.fullName,
                passwordHash: hash.keccak256(req.body.password),
                emailVerified: false,
                countryOfResidence: req.body.countryofresidence,
                nationality: req.body.nationality,
                gender: req.body.gender,
                addressOfResidence: req.body.addressofresidence,
                birthplace: req.body.birthplace,
                passportId: req.body.passportId,
                dateOfBirth: req.body.dateOfBirth,
                avatarUrl: "/defaultavatar"
            };
            try
            {
                
    
                var formData = {
                    AcceptTruliooTermsAndConditions: true,
                    CountryCode: 1,
                    PersonInfo: uinfo
                };
    
                //check with trulioo
                require('request').post(
                    {
                        url: "https://api.globaldatacompany.com/verifications/v1/verify",
                        formData: formData
                    }
                , function(error, response, body)
                {
                    var p = JSON.parse(body);
                    console.log(p);
                });
            }
            catch(E)
            {

            }
            
            db.collection("accounts").insert(uinfo);
            var r = fs.readFileSync("./html/regstat.html", "utf8").toString();
            r = r.replace("{messageTitle}", "Account Creation Successful");
            r = r.replace("{messageContents}", "An email has been sent to your account.<br>For additional verification, a physical mail was also sent to your address.<br>You have 3 days to confirm.<br><br><a href='/login'>Click here to return</a>");
            res.send(r);
            return;
        }
        //res.send(fs.readFileSync("./html/regstat.html", "utf8"));
     });
    
});

app.get('/main.css', (req, res)=>{
    res.type("text/css");
    res.contentType("text/css");
    res.send(fs.readFileSync("./css/main.css", "utf8"));
});

app.listen(3000, () => console.log('App listening on port 3000!'));