const mongoose = require('mongoose')
const passportLocalMongoose = require('passport-local-mongoose')

const userSchema = new mongoose.Schema({
  //_id: mongoose.Schema.ObjectId(),
  password: {
    type: String
  },
  email: {
    type: String
  },
  fullName: {
    type: String
  },
  countryofresidence: {
    type: String
  },
  nationality: {
    type: String
  },
  addressOfResidence: {
    type: String
  },
  birthplace: {
    type: String
  },
  passportId: {
    type: String
  },
  dateOfBirth: {
    type: String
  }
},{
  collection: 'user'
})

// userSchema.plugin(passportLocalMongoose)

module.exports = mongoose.model('user', userSchema)
