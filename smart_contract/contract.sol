/*
VoteSystem (C) veritas project 2018.
*/
pragma solidity ^0.4.24;

contract Election
{
    struct Voter
    {
        string fullName;
        string dateOfBirth;
        string nationality;
        string birthplace;
        string passportId;
        string gender;
        
        string countryOfResidence;
        string addressOfResidence;
        
        int availableVotes;
        bool registered;
        string id;
    }
    
    struct Candidate
    {
        bool registered;
        string name;
        int votes;
        string id;
    }
    
    string adminPassword = "";
    bool electionOpened = false;
    
    mapping (string => Voter) voters;
    mapping (string => Candidate) candidates;
    string[] registeredCandidates;
    uint256 currentIndex_rc = 0;
    
    function openElection(string adminPwd) public
    {
        require(keccak256(adminPassword) == keccak256(""));
        require(!electionOpened);
        adminPassword = adminPwd;
        electionOpened = true;
    }
    
    function registerVoter(string adminPass, string gender, string id, string fullName, string dateOfBirth, string nationality, string birthplace, string passportId, string language, string countryOfResidence, string addressOfResidence) public
    {
        require(electionOpened);
        require(keccak256(adminPassword) == keccak256(adminPass));
        require(!voters[id].registered);
        voters[id].fullName = fullName;
        voters[id].dateOfBirth = dateOfBirth;
        voters[id].nationality = nationality;
        voters[id].birthplace = birthplace;
        voters[id].passportId = passportId;
        voters[id].countryOfResidence = countryOfResidence;
        voters[id].addressOfResidence = addressOfResidence;
        voters[id].availableVotes = 1;
        voters[id].registered = true;
        voters[id].gender = gender;
        voters[id].id = id;
    }
    
    function voteForCandidate(string adminPass, string voterId, string candidateId) public
    {
        require(candidates[candidateId].registered);
        require(voters[voterId].registered);
        require(electionOpened);
        require(keccak256(adminPassword) == keccak256(adminPass));
        require(voters[voterId].availableVotes > 0);
        
        voters[voterId].availableVotes --;
        candidates[candidateId].votes ++;
    }
    
    function registerCandidate(string adminPass, string candidateId, string name) public
    {
        require(!candidates[candidateId].registered);
        require(keccak256(adminPassword) == keccak256(adminPass));
        require(electionOpened);
        candidates[candidateId].registered = true;
        candidates[candidateId].votes = 0;
        candidates[candidateId].name = name;
        candidates[candidateId].id = candidateId;
        registeredCandidates.push(candidateId);
        currentIndex_rc++;
    }
    
    function closeElection(string adminPass) public
    {
        require(keccak256(adminPassword) == keccak256(adminPass));
        electionOpened = false;
    }
    
    function getWinner(string adminPass) public returns (string)
    {
        require(electionOpened);
        require(keccak256(adminPassword) == keccak256(adminPass));
        string highestWinner = registeredCandidates[0];
        for(uint256 i=0; i<currentIndex_rc; i++)
        {
            if(candidates[highestWinner].votes <= candidates[registeredCandidates[i]].votes)
                highestWinner = candidates[highestWinner].id;
        }
        return highestWinner;
    }
    
    function getCandidateCount(string adminPass) public returns (uint256)
    {
        require(keccak256(adminPassword) == keccak256(adminPass));
        return currentIndex_rc;
    }
    
    function getCandidateId(string adminPass, uint256 index) public returns (string)
    {
        require(keccak256(adminPassword) == keccak256(adminPass));
        return registeredCandidates[index];
    }
    
    function getCandidateName(string adminPass, string id) public returns (string)
    {
        require(keccak256(adminPassword) == keccak256(adminPass));
        return candidates[id].name;
    }
    
    function getCandidateVotes(string adminPass, string id) public returns (int)
    {
        require(keccak256(adminPassword) == keccak256(adminPass));
        return candidates[id].votes;
    }
}