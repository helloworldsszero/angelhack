const passport = require('passport')
const bcrypt = require('bcrypt')
const LocalStrategy = require('passport-local').Strategy
const passportJWT = require('passport-jwt')
const ExtractJWT = passportJWT.ExtractJwt
const JWTStrategy = passportJWT.Strategy
const User = require('../server/models/user.model')

function validPassword (password, passwordHash) {
  return bcrypt.compareSync(password, passwordHash)
}
passport.use(new LocalStrategy(
  function (email, password, done) {
    User.findOne({
      email: email
    }, function (err, user) {
      if (err) {
        return done(err)
      }

      console.log(user)
      if (!user) {
        return done(null, false)
      }

      if (user.password !== password) {
        return done(null, false)
      }
      return done(null, user)
    })
  }
))

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'secret'
},
function (jwtPayload, cb) {
  console.log(jwtPayload)
  const item = jwtPayload
  // find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
  return User.findOne({
    email: item.email
  })
    .then(user => {
      return cb(null, user)
    })
    .catch(err => {
      return cb(err)
    })
}
))

module.exports = passport
