const express = require('express')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const User = require('../models/user.model')
// const userCtrl = require('../controllers/user.controller')
const loginCtrl = require('../controllers/login.controller')

// temp
const jwt = require('jsonwebtoken')
const passport = require('../../config/passport')

const router = express.Router()

//TODO return dashboard for user

router.route('/')
  .get((req, res) => {
    return res.json({
      hello: 'world'
    })
  })

router.route('/test')
  .get((req, res) => {
    passport.authenticate('jwt', { session: false }, (err, user) => {
      if (err) {
        console.log(err)
        return res.status(500).json({
          message: 'error'
        })
      }
      if (user) {
        return res.json({
          message: 'login success'
        })
      }
    })(req, res)
  })

router.route('/auth')
  .post((req, res) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
      if (err || !user) {
        return res.status(400).json({
          message: 'error.'
        })
      }

      req.login(user, {session: false}, (err) => {
        if (err) {
          res.send(err)
        }
        const item = {
          username: user.username,
          email: user.email
        }
        const token = jwt.sign(JSON.stringify(item), 'secret')
        return res.json({user, token})
      })
    })(req, res)
  })

router.post('/signup', function (req, res) {
  const user = new User({
    _id: new mongoose.Types.ObjectId(),
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  })
  user.save().then(function (result) {
    res.status(200).json({
      success: 'New user has been created'
    })
  }).catch(error => {
    res.status(500).json({
      error: err
    })
  })
})

router.route('/:loginId')

/** Load user when API with userId route parameter is hit */
router.param('loginId', loginCtrl.load)

module.exports = router
